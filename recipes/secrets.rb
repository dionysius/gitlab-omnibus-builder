chef_gem 'chef-vault' do
  compile_time true if respond_to?(:compile_time)
end
require 'chef-vault'

omnibus_builder_conf = GitLab::AttributesWithSecrets.get(node, 'gitlab-omnibus-builder')
username = omnibus_builder_conf['username']
builder_home = omnibus_builder_conf['home']

# Push access to packagecloud
template File.join(builder_home, '.packagecloud') do
  source 'packagecloud.erb'
  mode '0600'
  owner username
  group username
  variables omnibus_builder_conf
end

# SSH clone access to GitLab EE
ssh_dir = File.join(builder_home, '.ssh')

directory ssh_dir do
  mode '0700'
  owner username
  group username
end

file File.join(ssh_dir, 'id_rsa') do
  content omnibus_builder_conf['ee_source_deploy_privatekey']
  mode '0600'
  owner username
  group username
end

# Push access to AWS
aws_dir = File.join(builder_home, '.aws')

directory aws_dir do
  mode '0700'
  owner username
  group username
end

template File.join(aws_dir, 'config') do
  mode '0600'
  owner username
  group username
  variables omnibus_builder_conf
end
