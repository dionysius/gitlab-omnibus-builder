FROM resin/rpi-raspbian:wheezy
MAINTAINER Marin Jankovski <marin@gitlab.com>

# Prepare emulation scripts
# As seen on https://github.com/resin-io-projects/armv7hf-debian-qemu
ENV QEMU_EXECVE 1
COPY cross-build-end /usr/bin
COPY cross-build-start /usr/bin
COPY qemu-arm-static /usr/bin
COPY sh-shim /usr/bin

RUN [ "cross-build-start" ]

# Install required packages
RUN apt-get update -q
RUN DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      build-essential \
      zlib1g-dev \
      byacc \
      cmake \
      python-pip \
      git \
      gcc \
      libssl-dev \
      libyaml-dev \
      libffi-dev \
      libreadline-dev \
      libgdbm-dev \
      libncurses5-dev \
      make \
      bzip2 \
      curl \
      ca-certificates \
      locales \
      openssh-server \
      libcurl4-openssl-dev \
      libexpat1-dev \
      gettext \
      libz-dev \
      libssl-dev \
      fakeroot

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN pip install awscli

ENV GIT_VERSION 2.7.4
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-2.7.4 \
  && ls -la \
  && ./configure \
  && make all \
  && make install

ENV GO_VERSION 1.6.2
RUN curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-armv6l.tar.gz" \
	| tar -xzC /usr/local \
  && ln -sf /usr/local/go/bin/go /usr/local/go/bin/gofmt /usr/local/go/bin/godoc /usr/local/bin/

ENV RUBY_VERSION 2.1.8
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.1/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-2.1.8 \
  && ./configure --disable-install-rdoc \
  && make \
  && make install

ENV RUBYGEMS_VERSION 2.4.8
RUN /usr/local/bin/gem update --system ${RUBYGEMS_VERSION} --no-ri --no-rdoc

ENV BUNDLER_VERSION 1.9.9
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-ri --no-rdoc

RUN mkdir -p /opt/gitlab /var/cache/omnibus ~/.ssh

RUN git config --global user.email "packages@gitlab.com"
RUN git config --global user.name "GitLab Inc."

RUN rm -rf /tmp/*

RUN [ "cross-build-end" ]
