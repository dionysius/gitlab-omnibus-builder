default['gitlab-omnibus-builder']['username'] = 'gitlab_ci_multi_runner'
default['gitlab-omnibus-builder']['home'] = '/home/gitlab_ci_multi_runner'
default['gitlab-omnibus-builder']['projects'] = %w{gitlab}
default['gitlab-omnibus-builder']['ruby_version'] = '2.1.6'
default['gitlab-omnibus-builder']['rubygems_version'] = '2.4.8'
default['gitlab-omnibus-builder']['bundler_version'] = '1.9.9'
default['gitlab-omnibus-builder']['ee_source_host_key'] = "dev.gitlab.org,109.107.38.152 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCutTK+e1IDw7eE+HXrhGLh0Alk/pHAnjFVVFhgkNembw3bfq3pBkW1p+kwbjy0dwNOMw45AugRzYnUxVa5OpYn2fcjV6nIl//5IQpqFeVsUfW2dhHVSVS+ojO5KhgRSzsS0z5mcjA1Vj/UAb7iS0xsiI37fL8szS2IO72RT3AsEPzJJ3oFANwJQVL1z6wQ3HKQ8V5ctcp9vWjs76gkxuckRw2+TFLthQyxT4vP/J7UbGjdYfdeplgsLni0L1wXv6gxeIRt/PciOOHmqEmHEcmzPBuSr/3RzlTTxE9mdOq8cO9xYGCrEzs7R/hrp6KUjeFqDoEnGUE91m3iCQyv6qtb"
default['gitlab-omnibus-builder']['ee_source_deploy_privatekey'] = 'secret'
default['gitlab-omnibus-builder']['packagecloud_url'] = 'https://packages.gitlab.com'
default['gitlab-omnibus-builder']['packagecloud_token'] = 'secret'
default['gitlab-omnibus-builder']['aws_access_key_id'] = 'secret'
default['gitlab-omnibus-builder']['aws_secret_access_key'] = 'secret'

default['gitlab-omnibus-builder']['go_version'] = '1.5.4'
default['gitlab-omnibus-builder']['go_tarball'] = "go#{node['gitlab-omnibus-builder']['go_version']}.linux-amd64.tar.gz"
default['gitlab-omnibus-builder']['go_tarball_sha256'] = 'a3358721210787dc1e06f5ea1460ae0564f22a0fbd91be9dcd947fb1d19b9560'

default['gitlab-omnibus-builder']['git_version'] = '2.7.4'
default['gitlab-omnibus-builder']['git_tarball'] = "git-#{node['gitlab-omnibus-builder']['git_version']}.tar.gz"
default['gitlab-omnibus-builder']['git_tarball_sha256'] = '7104c4f5d948a75b499a954524cb281fe30c6649d8abe20982936f75ec1f275b'
